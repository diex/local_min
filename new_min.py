from random import randint
from itertools import accumulate

def local_min(A):
    n=len(A)
    local_min_aux(A,1,n)

def local_min_aux(A,inicio,fin):
    if inicio>=fin:
        return 0
    pos=(fin+inicio)//2
    if pos<1 or pos>=fin or len(A)<3:
        return 0
    if A[pos-1]>=A[pos] and A[pos]<=A[pos+1]:
        print(pos,A[pos])
        return pos
    else:
        if local_min_aux(A,inicio,pos)==0:
            return local_min_aux(A,pos+1,fin)
        
        
        
        
              

def randomwalk(N):
    W = [ randint(-1, 1) for _ in range(N)]
    A = list(accumulate(W))
    return A

if __name__=='__main__':
    serie = randomwalk(1000)
    #print(serie)
    A=[0,1,2,3,4,5,7,8,9,8,7,6,5,4,3,2,3,8,1,8,7,6,5,14,8,0]
    local_min(A)

